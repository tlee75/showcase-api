FROM python:3.8.6-slim

COPY . /src/showcase
WORKDIR /src/showcase

RUN pip install -r requirements.txt --src /usr/local/src

RUN chmod +x ./start_api.sh
CMD ["./start_api.sh"]