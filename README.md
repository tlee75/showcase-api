#showcase-api

This is a simple API that will be built into an image and pushed to the Amazon ECR.

## Build
`docker build -t showcase-api:latest .`

## Run

`docker run -it -p 5000:5000 showcase-api`