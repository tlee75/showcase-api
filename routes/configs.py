from flask import Blueprint, jsonify

configs_route = Blueprint('configs_route', __name__,)

@configs_route.route('/configs')
def retrieveConfig():

	response= {
		'url': "https://www.google.com"
	}

	return jsonify(response)
