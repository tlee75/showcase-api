from flask import Blueprint
import json

index_route = Blueprint('index_route', __name__,)

@index_route.route('/')
def index():
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'}


