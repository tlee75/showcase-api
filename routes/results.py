from flask import Blueprint, jsonify

results_route = Blueprint('results_route', __name__,)

@results_route.route('/results')
def retrieveResult():

	response = {
		'ping': 100
	}

	return jsonify(response)
