from flask import Flask
from routes.index import index_route
from routes.results import results_route
from routes.configs import configs_route


app = Flask(__name__)
app.register_blueprint(index_route)
app.register_blueprint(results_route)
app.register_blueprint(configs_route)

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=False)